const nav = document.getElementById('nav-links')
const burgerButton = document.getElementById('burger-menu')
const links = document.querySelectorAll('a')

burgerButton.addEventListener('click', () => {
    links.forEach(link => {
        link.classList.toggle('visible');
    })
    if (nav.classList.contains('toggler')) {
        nav.classList.remove('toggler');
    } else {
        nav.classList.add('toggler');
    }
});