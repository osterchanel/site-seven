<?php require_once('./head.php')
?>
<link rel="stylesheet" href="./assets/css/index.css">
</head>
<?php
require_once('./header.php')
?>

<body>
    <?php
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
        switch ($action) {
            default:
                break;
            case 'a':
                include('./templates/aPropos.php');
                break;
            case 'p':
                include('./templates/prestation.php');
                break;
            case 'r':
                include('./templates/realisation.php');
                break;
            case 'c':
                include('./templates/contact.php');
                break;
        }
    } else {
    ?>
    <div class="presentation">
        <img src="./assets/images/lpd.jpg" alt="Le quartier de la part dieu">
        <div class="slogan">
            <p>Osez, <br><span>Nous innovons!</span></p>
        </div>
        <div class="orange-rectangle"></div>
    </div>
    <?php
    }
    require_once('./footer.php')
    ?>