<header id="nav">
    <a href="index.php"><img src="./assets/images/Seven-logo.png" alt="Logo de Seven" class="logo"></a>
    <nav id="nav-links">
        <a href="index.php?action=a">A propos de nous</a>
        <a href="index.php?action=p">Nos prestations</a>
        <a href="index.php?action=r">Nos réalisations</a>
        <a href="index.php?action=c">Nous contacter</a>
    </nav>
    <img src="./assets/images/menu_FILL0_wght400_GRAD0_opsz48.png" alt="Menu burger" class="burger-menu" id="burger-menu">
</header>
<div class="social-network">
    <img src="./assets/images/icons8-facebook-48.png" alt="Facebook">
    <img src="./assets/images/icons8-instagram-48.png" alt="Instagram">
    <img src="./assets/images/icons8-linkedin-48.png" alt="Facebook">
</div>